package com.skype.mediagallery;

import android.net.Uri;

/**
 * Created by ss on 10/15/17.
 */

public class MediaViewModel {

    private final Uri path;
    private boolean isSelected;

    public MediaViewModel(Uri path) {
        this.path = path;
        isSelected = false;
    }

    public Uri getPath() {
        return path;
    }

    public synchronized boolean isSelected() {
        return isSelected;
    }

    public synchronized void toggleSelected() {
        isSelected = !isSelected;
    }

}
