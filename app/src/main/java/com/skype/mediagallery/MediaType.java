package com.skype.mediagallery;

/**
 * Created by ss on 10/15/17.
 */

public final class MediaType {

    public static final int IMAGE = 1;
    public static final int VIDEO = 2;
}
