package com.skype.mediagallery;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Iterator;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ss on 10/15/17.
 */

public class MainTest extends AppCompatActivity {

    public static final int REQUEST_CODE = 100;

    @BindView(R.id.result_text)
    TextView resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_test);
        ButterKnife.bind(this);

        Intent i = new Intent(MainTest.this, Main.class);
        startActivityForResult(i, REQUEST_CODE);
    }

    @SuppressWarnings("StringConcatenationInsideStringBufferAppend")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Please ignore Lint errors below.

        StringBuilder sb = new StringBuilder();
        sb.append("Result: " + (resultCode == Activity.RESULT_OK ? "Result_OK\n" : "Bad result code:" + resultCode) + "\n");
        Bundle bundle = data.getExtras();
        Set<String> keySet = bundle.keySet();
        Iterator<String> iterator = keySet.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            Object[] uris = (Object[]) bundle.get(key);
            sb.append("\n\n" + key + "\n");
            for (Object uri : uris) {
                sb.append(((Uri) uri).getPath() + "\n");
            }
        }

        resultText.setText(sb.toString());
    }


}