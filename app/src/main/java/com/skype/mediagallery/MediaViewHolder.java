package com.skype.mediagallery;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ss on 10/15/17.
 */

public class MediaViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.media_image)
    ImageView content;

    @BindView(R.id.media_image_selected)
    ImageView selector;

    public ImageView getSelector() {
        return selector;
    }


    public MediaViewHolder(View root) {
        super(root);
        ButterKnife.bind(this, root);
    }

    public ImageView getContent() {
        return content;
    }

}
