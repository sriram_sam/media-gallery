package com.skype.mediagallery;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;

import io.reactivex.ObservableEmitter;

/**
 * Created by ss on 10/14/17.
 */

class PhotoAdapter extends BaseMediaAdapter {

    public PhotoAdapter() {
        externalUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }


    @Override
    public void fetchContent(RecyclerView recyclerView, ObservableEmitter<Uri> emitter) {
        context = recyclerView.getContext();

        Cursor cursor = context.getContentResolver().
                query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Images.Thumbnails.IMAGE_ID},
                        null,
                        null,
                        null);

        int imageColumnIndex = cursor.getColumnIndex(MediaStore.Images.Thumbnails.IMAGE_ID);

        while (cursor.moveToNext()) {
            String path = cursor.getString(imageColumnIndex);
            Uri uri = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, path);
            emitter.onNext(uri);
        }
        cursor.close();
        emitter.onComplete();
    }

    @Override
    protected int getType() {
        return MediaType.IMAGE;
    }

}
