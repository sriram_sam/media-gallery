package com.skype.mediagallery;

import android.net.Uri;

/**
 * Created by ss on 10/15/17.
 */

public class SelectionEvent extends Event {
    int action;
    Uri uri;
}
