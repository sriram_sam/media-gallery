package com.skype.mediagallery;

import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by ss on 10/15/17.
 */

public final class Bus {

    private static Subject<Event> bus;

    public static Subject<Event> get() {
        if (bus == null) {
            bus = PublishSubject.create();
        }
        return bus;
    }

    private Bus() {
    }
}
