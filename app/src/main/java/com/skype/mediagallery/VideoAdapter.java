package com.skype.mediagallery;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;

import io.reactivex.ObservableEmitter;

/**
 * Created by ss on 10/14/17.
 */

class VideoAdapter extends BaseMediaAdapter {


    public VideoAdapter() {
        externalUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
    }

    @Override
    public void fetchContent(RecyclerView recyclerView, ObservableEmitter<Uri> emitter) {
        context = recyclerView.getContext();

        Cursor cursor = context.getContentResolver().
                query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI,
                        new String[]{MediaStore.Video.Thumbnails.VIDEO_ID},
                        null,
                        null,
                        null);

        int imageColumnIndex = cursor.getColumnIndex(MediaStore.Video.Thumbnails.VIDEO_ID);

        while (cursor.moveToNext()) {
            String path = cursor.getString(imageColumnIndex);
            Uri uri = Uri.withAppendedPath(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, path);
            emitter.onNext(uri);
        }
        cursor.close();
        emitter.onComplete();
    }

    @Override
    protected int getType() {
        return MediaType.VIDEO;
    }
}
