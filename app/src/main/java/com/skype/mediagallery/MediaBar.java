package com.skype.mediagallery;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;

/**
 * Created by ss on 10/14/17.
 */

public class MediaBar extends Fragment {

    private int type;

    @BindView(R.id.title_text)
    TextView title;

    @BindView(R.id.subtext)
    TextView subtext;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    final Subject<Event> bus;
    final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public MediaBar() {
        this.bus = Bus.get();
    }

    int totalElements = 0;
    int selectedElements = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View root = inflater.inflate(R.layout.media_bar_layout, container, false);
        ButterKnife.bind(this, root);
        setTextContent(title, type);
        prepareRecyclerView(type);
        subscribeToBus();
        return root;
    }

    private void subscribeToBus() {
        Disposable d = bus
                .observeOn(AndroidSchedulers.mainThread())
                .filter(event -> event.type == type)
                .subscribe(event -> {

                    if (event instanceof SizeChangedEvent) {
                        SizeChangedEvent sizeChangedEvent = (SizeChangedEvent) event;
                        setTotalElements(sizeChangedEvent.newSize);
                    } else if (event instanceof SelectionEvent) {
                        SelectionEvent selectionEvent = (SelectionEvent) event;
                        selectedElements += selectionEvent.action;
                    }
                    updateSubText();
                });


        compositeDisposable.add(d);
    }

    private void updateSubText() {
        subtext.setText(String.format(Locale.ENGLISH, "%d of %d selected", selectedElements, totalElements));
    }

    public synchronized void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        compositeDisposable.dispose();
    }

    private void prepareRecyclerView(int type) {

        RecyclerView.Adapter adapter = getAdapter(type);
        recyclerView.setAdapter(adapter);
    }

    private RecyclerView.Adapter getAdapter(int type) {
        switch (type) {
            case MediaType.IMAGE:
                return new PhotoAdapter();
            case MediaType.VIDEO:
                return new VideoAdapter();
            default:
                throw new RuntimeException("Invalid case");
        }
    }

    private void setTextContent(TextView view, int type) {

        switch (type) {
            case MediaType.IMAGE:
                view.setText(R.string.Photos_Label);
                break;
            case MediaType.VIDEO:
                view.setText(R.string.Videos_Label);
                break;
            default:
                throw new RuntimeException("Invalid type: " + type);
        }

    }

    @Override
    public void onInflate(Context context, AttributeSet attrs, Bundle savedInstanceState) {
        super.onInflate(context, attrs, savedInstanceState);
        TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.MediaBar);
        type = styledAttributes.getInteger(R.styleable.MediaBar_content_type, -1);
        if (type == -1) {
            throw new RuntimeException("Content Type attribute needed in xml");
        }
        styledAttributes.recycle();
    }
}
