package com.skype.mediagallery;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public class Main extends AppCompatActivity {

    public static final String EXTRA_IMAGE_URIS = "IMAGE_URIs";
    public static final String EXTRA_VIDEO_URIS = "VIDEO_URIs";

    private final int REQUEST_CODE = 1;

    final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (ContextCompat.checkSelfPermission(Main.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            setContentView(R.layout.default_layout);

            ActivityCompat.requestPermissions(Main.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE);
        } else {
            setMediaContent();
        }

        subscribeToBus();
    }

    final Set<Uri> imageSet = new HashSet<>();
    final Set<Uri> videoSet = new HashSet<>();

    private void subscribeToBus() {
        Bus.get()
                .observeOn(AndroidSchedulers.mainThread())
                .filter(event -> event instanceof SelectionEvent)
                .subscribe(event -> {

                    switch (event.type) {
                        case MediaType.IMAGE:
                            handle((SelectionEvent) event, imageSet);
                            break;
                        case MediaType.VIDEO:
                            handle((SelectionEvent) event, videoSet);
                            break;
                    }

                });
    }

    private synchronized void handle(SelectionEvent event, Set<Uri> set) {
        switch (event.action) {
            case Action.SELECTED:
                set.add(event.uri);
                break;
            case Action.UNSELECTED:
                set.remove(event.uri);
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            setMediaContent();
        }
    }

    private void setMediaContent() {
        setContentView(R.layout.main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.fab)
    public synchronized void send() {

        Intent intent = new Intent();
        intent.putExtra(EXTRA_IMAGE_URIS, imageSet.toArray());
        intent.putExtra(EXTRA_VIDEO_URIS, videoSet.toArray());
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
