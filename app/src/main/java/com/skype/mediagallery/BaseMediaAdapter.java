package com.skype.mediagallery;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.Subject;

/**
 * Created by ss on 10/14/17.
 */

public abstract class BaseMediaAdapter extends RecyclerView.Adapter {

    final CompositeDisposable subscriptions = new CompositeDisposable();
    final List<MediaViewModel> mediaList = new ArrayList<>();
    Context context;
    private final Subject<Event> bus;

    public BaseMediaAdapter() {
        this.bus = Bus.get();
    }

    @Override
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        Disposable d = Observable.create((ObservableEmitter<Uri> emitter) -> {
            fetchContent(recyclerView, emitter);
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(newMedia -> {
                    if (!mediaList.contains(newMedia)) {

                        mediaList.add(new MediaViewModel(newMedia));
                        notifyItemInserted(mediaList.size() - 1);

                        SizeChangedEvent event = new SizeChangedEvent();
                        event.type = getType();
                        event.newSize = mediaList.size();

                        bus.onNext(event);
                    }
                });

        subscriptions.add(d);

    }

    public abstract void fetchContent(RecyclerView recyclerView, ObservableEmitter<Uri> emitter);


    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        context = null;
        subscriptions.clear();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View root = inflater.inflate(R.layout.recycler_item, parent, false);
        return new MediaViewHolder(root);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MediaViewModel viewModel = mediaList.get(position);
        MediaViewHolder viewHolder = (MediaViewHolder) holder;

        Uri mediaPath = viewModel.getPath();

        Glide.with(context)
                .load(mediaPath)
                .apply(new RequestOptions()
                        .placeholder(android.R.drawable.ic_menu_gallery)
                        .diskCacheStrategy(DiskCacheStrategy.ALL))

                .into(viewHolder.getContent());

        int visibility = viewModel.isSelected() ? View.VISIBLE : View.INVISIBLE;
        viewHolder.getSelector().setVisibility(visibility);
        viewHolder.getContent().setOnClickListener((View view) -> {

            viewModel.toggleSelected();

            int v = viewModel.isSelected() ? View.VISIBLE : View.INVISIBLE;
            viewHolder.getSelector().setVisibility(v);

            SelectionEvent event = new SelectionEvent();
            event.action = viewModel.isSelected() ? Action.SELECTED : Action.UNSELECTED;
            event.uri = getUriForAsset(mediaPath.getLastPathSegment());
            event.type = getType();
            bus.onNext(event);

        });
    }

    protected Uri externalUri;

    protected Uri getUriForAsset(String path) {
        return Uri.withAppendedPath(externalUri, path);
    }

    protected abstract int getType();

    @Override
    public int getItemCount() {
        //TODO conceive a UI when mediaList.size() = 0
        return mediaList.size();
    }
}
